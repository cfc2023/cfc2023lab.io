import { Reference } from "@src/lib/reference";

export function isIntersecting(
  element: HTMLElement,
  isIntersectingReference: Reference<boolean>
) {
  let observer: IntersectionObserver;

  observer = new IntersectionObserver(
    (entries) => isIntersectingReference.set(!entries[0].isIntersecting),
    {
      root: null,
      rootMargin: "0px",
      threshold: 0,
    }
  );

  observer.observe(element);
  return {
    destroy() {
      observer.disconnect();
    },
  };
}
