import type { CourseConfig } from "./_course-config.model";

const courseList: CourseConfig[] = [
  {
    cardsDir: "@src/content/courses/cfrm",
    frenchSlug: "championnats-de-france-relais-sprint",
    englishSlug: "sprint-relay-french-championships",
    frenchTitle: "CFRS",
    frenchLongTitle: "Championnat de France de Relais Sprint",
    englishTitle: "CFRS",
    englishLongTitle: "Sprint Relay French Championships",
    date: "2023-05-18",
  },
  {
    cardsDir: "@src/content/courses/regional",
    frenchSlug: "regionale-md",
    englishSlug: "regional-md",
    frenchTitle: "Régionale MD",
    frenchLongTitle: "Régionale Moyenne Distance",
    englishTitle: "Regional MD",
    englishLongTitle: "Regional Middle Distance",
    date: "2023-05-19",
  },
  {
    cardsDir: "@src/content/courses/cfmd",
    frenchSlug: "championnats-de-france-moyenne-distance",
    englishSlug: "middle-distance-french-championships",
    frenchTitle: "CFMD",
    frenchLongTitle: "Championnats de France de Moyenne Distance",
    englishTitle: "CFMD",
    englishLongTitle: "Middle Distance French Championships",
    date: "2023-05-20",
  },
  {
    cardsDir: "@src/content/courses/cfc",
    frenchSlug: "championnats-de-france-des-clubs",
    englishSlug: "clubs-french-championships",
    frenchTitle: "CFC",
    frenchLongTitle: "Championnats de France des Clubs",
    englishTitle: "CFC",
    englishLongTitle: "Clubs French Championships",
    date: "2023-05-21",
  },
];

export default courseList;
