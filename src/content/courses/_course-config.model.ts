export interface CourseConfig {
  cardsDir: string;
  frenchSlug: string;
  englishSlug: string;
  frenchTitle: string;
  frenchLongTitle: string;
  englishTitle: string;
  englishLongTitle: string;
  date: string;
}
