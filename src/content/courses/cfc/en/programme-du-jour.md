---
order: 1
draft: false
---
05:30 : Reception opening

06:30 : Mass-start N1

06:40 : Mass-start N2

07:00 : Mass-start N3

07:30 : Mass-start N4

08:00 : Mass-start TTG

08:20 : Mass-start Relais Retord

12:30 : Prize-giving ceremony

1﻿4:30 : Competition stops (controls are picked out).

*Conformément au règlement, l’horaire de départ en masse des retardataires sera défini sur décision de l’arbitre et annoncé au micro.*