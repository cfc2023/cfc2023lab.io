---
title: Results, pictures and analysis
order: 0
draft: false
---

**Official results:** [pdf](/images/uploads/resultats_officiels_cfc.pdf)

**Split times :** [pdf](/images/uploads/cfc-splits.pdf)

**Analysis :** [Livelox](https://www.livelox.com/Events/Show/96106/Championnat-de-France-des-Clubs-2023)

[**Pictures**](https://photos.app.goo.gl/MBn7yPSS2wu14gEWA)
