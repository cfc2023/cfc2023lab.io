---
title: Start lists and results
order: 2
draft: false
---

**Start lists:** [per division](/images/uploads/liste-de-départ-par-division_cfc.pdf) / [per club](/images/uploads/liste-de-départ-par-club_cfc.pdf) (﻿version: 19/05/2023 22:00)

**Live results:** [CFC](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=7)

**Official results:**

**Analysis:** [Livelox](https://www.livelox.com/Events/Show/96106/Championnat-de-France-des-Clubs-2023)
