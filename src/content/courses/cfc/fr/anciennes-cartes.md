---
title: Anciennes cartes
order: 4
draft: false
---

![Extrait de carte](/images/uploads/extraitrect1..png "Extrait de carte du Tumet")

- [Le Tumet (2020)](/images/uploads/le-tumet_7500_oocup2020.png)
- [Plateau de Retord (2008)](/images/uploads/plateau-de-retord_20000_2008.jpg)

\_﻿\_\_\_\_\_\_

L﻿a carte de la zone de course a été générée à partir des données Lidar par Mapant.fr. A retrouver [ici](http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U).

<a href="http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U" > <img src="/images/uploads/mapant.png" alt="logo_MapantFR" style="width: 60%; margin: auto;"></a>
