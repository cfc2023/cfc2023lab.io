---
title: Informations sur le CFC
order: 1
draft: false
---
L﻿a liste des clubs qualifiés est disponible sur le [site de la FFCO](https://www.ffcorientation.fr/courses/qualifications/).

V﻿ous n'avez pas d'équipe pour le CFC ? Vous pouvez tout de même participer au relais Retord !
**Le relais Retord** est un relais ouvert à tous, y compris aux non-licenciés. Il est composé de 3 circuits violet d'environ 4,5km).

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a href="/images/uploads/230521_annonce-de-course_cfc_v01.pdf" style="display:contents;"> <img src="/images/uploads/txt_annoncecourse.svg" alt="annonce de course" style="height: 80%;"></a>
</p>
