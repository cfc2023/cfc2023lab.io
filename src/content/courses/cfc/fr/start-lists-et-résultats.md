---
title: Listes de départ et résultats live
order: 2
draft: false
---
**Listes de départ :** [par division](/images/uploads/liste-de-départ-par-division_cfc.pdf) / [par club](/images/uploads/liste-de-départ-par-club_cfc.pdf) (﻿version du 19/05/2023 22:00)

**Résultats live :** [CFC](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=7)
