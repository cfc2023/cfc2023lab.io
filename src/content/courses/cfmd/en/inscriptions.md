---
title: Registration
order: 5
draft: false
---
* Registration for license holders of a french orienteering club and foreign competitors: on the [FFCO website](https://licences.ffcorientation.fr/inscriptions/2580/).
* L﻿eisure participants: All informations on the [Leisure page](https://cfc2023.fr/en/Leisure/).
* World Ranking Event (M21/W21) : Registration on [Eventor](https://eventor.orienteering.org/Events/Show/7651). (more information in the WRE bulletins)

*Note that competitors who want to get a WRE ranking must also enter the race on eventor.*