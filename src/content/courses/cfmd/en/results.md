---
title: Results, pictures and analysis
order: 0
draft: false
---

**Official results:** [per classes](/images/uploads/cfmd-resultats-categories.pdf)

**Analysis:** [Livelox](https://www.livelox.com/Events/Show/96103/Championnat-de-France-Moyenne-Distance-2023-open-)

**Split times:** [pdf](/images/uploads/cfmd-splits-categories.pdf) / [Winsplits](http://obasen.orientering.se/winsplits/online/fr/default.asp?page=classes&databaseId=89717) / [splittimes (experimental app)](https://splittimes.pages.dev/file-url/1?file-url=https%3A%2F%2Fcfc2023.fr%2Fsplits-cfmd)

[**Pictures**](https://photos.google.com/share/AF1QipNbVcJ1eotZesWRGx16qVFREsZNw281920UjxWB3W0w1na3uZiTgamOoNGWgyF_ow?pli=1&key=UWM2ZTJRVFV0Wk1VREVIWkgyNXg2OVBnYm40SnhB)

