---
title: Start lists and live results
order: 2
draft: false
---
**Start lists:**
[﻿per category](/images/uploads/heures-de-départ-par-catégorie-_cfmdetopen.pdf) / [per club](/images/uploads/heures-de-départ-par-club-_cfmdetopen.pdf)

18/05/2023 - 23h55 : **⚠ Startlist for races A & B changed.**

**Live results:** [﻿CFMD et open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=5)
