---
title: Informations sur les Championnats de France de Moyenne Distance
order: 3
draft: false
---
20/05/2022
Plateau de Retord **(nouvelle carte)**

C﻿ourse comptant pour le Classement National et World Ranking Event pour les courses élites (D21 et H21).

L﻿a liste des coureurs qualifiés pour les championnats de France de Moyenne Distance est disponible sur le [site de la FFCO](https://www.ffcorientation.fr/courses/qualifications/).

C﻿ourses open pour les coureurs non qualifiés au championnat de France et aux participants non licenciés.

<p style="display: grid; grid-template-columns: auto auto auto; justify-items: center;align-items: center; gap: 1rem;"><a style="height: 100%;" href="/images/uploads/bulletin2_en_v1.pdf"><img style="height: 100%;" src="/images/uploads/txt_bulletinwre2.svg" alt="BulletinWRE"></a><a style="height: 100%;" href="/images/uploads/bulletin-1_en_v3.pdf"><img style="height: 100%;" src="/images/uploads/txt_bulletinwre1.svg" alt="BulletinWRE"></a><a style="height: 100%;" href="/images/uploads/230520_annonce-de-course_cfmd_v01.pdf"> <img style="height: 100%;" src="/images/uploads/txt_annoncecourse.svg" alt="annonce de course"></a></p>