---
title: Programme du jour
order: 1
draft: false
---
08:30 Ouverture de l'accueil

9:45 - 13:30 Départs compétiteurs CFMD (horaires attribués par l’organisateur,
selon classement WRE ou CN)

14:00 Premiers départs des circuits loisirs (horaires à choisir par les participants)

13:30 - 17:00 Départs compétiteurs Open (horaires attribués par l’organisateur)

14:00 Remise des prix du championnat de France MD

Fermeture des circuits et débalisage : 1h30 après le dernier départ
