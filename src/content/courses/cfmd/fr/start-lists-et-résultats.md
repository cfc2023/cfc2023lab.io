---
title: Listes de départ et résultats live
order: 2
draft: false
---
**H﻿oraires de départ** (﻿CFMD et open) :
[﻿par catégorie](/images/uploads/heures-de-départ-par-catégorie-_cfmdetopen.pdf) / [par club](/images/uploads/heures-de-départ-par-club-_cfmdetopen.pdf)

18/05/2023 - 23h55 : **⚠** **A﻿ttention, les horaires de départs pour les circuits A et B ont changé.**

**Résultats live :** [﻿CFMD et open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=5)