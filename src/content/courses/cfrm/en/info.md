---
title: Informations about Sprint Relay French Championships
order: 1
draft: false
---

18/05/2022 (Afternoon)
P﻿lace: Oyonnax - Bellignat

T﻿eam of 4 teammates (2 men and 2 women)
Reserved to qualified french clubs.

**A﻿n individual sprint race will take place after the sprint relay, in the same area. Open to all runners.**

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a data-router-ignore href="/images/uploads/230518_annonce-de-course_cfrs_v01.pdf" style="display:contents;"><img src="/images/uploads/txt_annoncecoursers.svg" alt="BulletinWRE" style="height: 80%;"></a><a href="/images/uploads/230518_annonce-de-course_regionalesprint_v01.pdf" style="display:contents;"> <img src="/images/uploads/txt_annoncecourseso.svg" alt="annonce de course" style="height: 80%;"></a>
</p>