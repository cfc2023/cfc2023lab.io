---
title: Registration
order: 5
draft: false
---
**Sprint Relay :** (only for french clubs) Registration on the [FFCO website](https://licences.ffcorientation.fr/inscriptions/2579/).

**Sprint :**

* Registration for c﻿ompetitors : on the [FFCO website](https://licences.ffcorientation.fr/inscriptions/2584/).
* Leisure participants: All informations on the \[Leisure page](https://cfc2023.fr/en/Leisure/).