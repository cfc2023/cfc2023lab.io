---
title: Day program
order: 2
draft: false
---
10:00 Reception opening

13:00 Quarantine closes (for sprint relay runners)

14:00 Sprint relay starts

1﻿6:00 Prize-giving ceremony of the sprint relay

1﻿6:00 First start of the open sprint race

1﻿6:30-18:00 Starts for the leisure participants (non licensed)

1﻿9:00 Competition stops (controls are picked out).