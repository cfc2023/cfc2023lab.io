---
title: Results, pictures and analysis
order: 0
draft: false
---

**Split times:** [WinSplits CFRS](http://obasen.orientering.se/winsplits/online/fr/default.asp?page=classes&databaseId=89644)

**Analysis:** [Livelox CFRS](https://www.livelox.com/Events/Show/96072/Championnat-de-France-Relais-Sprint-2023) / [Livelox sprint open](https://www.livelox.com/Events/Show/96084/Sprint-Open-CFRS-)

**Official results:** [CFRS](public-assets/images/uploads/resultats_cfrs_clubs.pdf) / [Sprint by category](/images/uploads/resultats_opensprint_categorie.pdf)

**Pictures:** [CFRS](https://photos.app.goo.gl/Y81iy8egVtJGT7Ne8)