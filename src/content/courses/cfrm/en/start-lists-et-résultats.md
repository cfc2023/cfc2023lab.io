---
title: Start lists and live results
order: 3
draft: false
---
**Start lists:** 

* [Startlist C﻿FRS](/images/uploads/equipes_cfrs.pdf).  S﻿print relay starts at 14:00.
* [Sprint open, per course](/images/uploads/heures-de-départ-par-circuits-_sprintopen.pdf) / [Sprint open, per club](/images/uploads/heures-de-départ-par-club-_sprintopen.pdf)

**Live results:** [CFRS](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=1) / [Sprint open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=3)

<p><a href="https://ffcorientation.routechoices.com/cfrs2023"><strong>GPS tracking</strong></a> (10 teams will be equiped with GPS trackers)</p>

<p><a href="https://www.youtube.com/@coursedorientation"><strong>Youtube</strong></a> (Experimentation: shared views from the arena)</p>
