---
title: Informations - Championnat de France de Relais Sprint (CFRS)
order: 1
draft: false
---

18/05/2022 - après-midi
Oyonnax - Bellignat

E﻿quipes de 4 personnes (2 hommes et 2 femmes).
L﻿a liste des clubs qualifiés est disponible sur le [site de la FFCO](https://www.ffcorientation.fr/courses/qualifications/).

U﻿n sprint open, ouvert à tous, aura lieu après le CFRS, sur la même zone.

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a data-router-ignore href="/images/uploads/230518_annonce-de-course_cfrs_v01.pdf" style="display:contents;"><img src="/images/uploads/txt_annoncecoursers.svg" alt="BulletinWRE" style="height: 80%;"></a><a href="/images/uploads/230518_annonce-de-course_regionalesprint_v01.pdf" style="display:contents;"> <img src="/images/uploads/txt_annoncecourseso.svg" alt="annonce de course" style="height: 80%;"></a>
</p>

