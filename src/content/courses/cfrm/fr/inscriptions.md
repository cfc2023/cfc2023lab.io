---
title: Inscriptions
order: 5
draft: false
---
**Relais sprint :** inscriptions sur le [site de la FFCO](https://licences.ffcorientation.fr/inscriptions/2579/).

**Régionale Sprint :**

* Pour les compétiteurs : inscriptions sur le [site de la FFCO](https://licences.ffcorientation.fr/inscriptions/2584/).
* Participants l﻿oisir : toutes les informations sur la page [Loisir](https://cfc2023.fr/fr/Loisir/).