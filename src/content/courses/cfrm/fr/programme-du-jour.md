---
title: Programme du jour
order: 2
draft: false
---
10:00 : Ouverture de l’accueil sur l'arena

1﻿3:00 : Fermeture de la quarantaine (CFRS)

14:00 : Départ du CFRS

1﻿6:00 : Remise des prix CFRS

1﻿6:00 : Premier départ du sprint open (horaires attribués par l'organisateur)

1﻿6:30-18:00 : Départs des circuits loisir (non licenciés)

1﻿9:00 : Fermeture des circuits