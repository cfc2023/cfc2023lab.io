---
title: Listes de départ et résultats live
order: 3
draft: false
---
**Listes et horaires de départ :**

* [Equipes C﻿FRS](/images/uploads/equipes_cfrs.pdf)  : Départ en masse à 14:00.
* [Sprint open, par circuit](/images/uploads/heures-de-départ-par-circuits-_sprintopen.pdf) / [Sprint open, par club](/images/uploads/heures-de-départ-par-club-_sprintopen.pdf)

**Résultats live :** [CFRS](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=1) / [Sprint open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=3)

<p><a href="https://ffcorientation.routechoices.com/cfrs2023"><strong>Suivi GPS</strong></a> (10 équipes seront équipés de GPS)</p>

<p><a href="https://www.youtube.com/@coursedorientation"><strong>Youtube</strong></a> (images de l'arena, proposées à titre expérimental)</p>
