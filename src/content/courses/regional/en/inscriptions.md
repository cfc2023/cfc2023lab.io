---
title: Registration
order: 6
draft: false
---
* Registration for competitors: [FFCO website](https://licences.ffcorientation.fr/inscriptions/2578/).
* Leisure participants: All informations on the [Leisure page](https://cfc2023.fr/en/Leisure/).