---
title: Start lists, results & pictures
order: 0
draft: false
---
Race "jalonné" : start with punching, no startlist, free start between 11:30 and 16:00.

**Official results:** [per race](/images/uploads/resultats_régionnale.pdf)

**Analysis:** 

* GPS : [Livelox](https://www.livelox.com/Events/Show/96101/Regionale-MD-Le-Tumet-)
* Temps intermédiaires : [WinSplits](http://obasen.orientering.se/winsplits/online/fr/default.asp?page=classes&databaseId=89649&ct=true) / [Split times (experimental application)](https://splittimes.pages.dev/winsplits/89649)

**[Pictures](https://photos.app.goo.gl/QhLXhqfKStWsucg77)**

**Live results:** [Regional](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=4)

**Start lists:** [per club](/images/uploads/heures-de-départ-par-club-regionaleMD.pdf) / [per course](/images/uploads/heures-de-départ-par-circuit-regionaleMD.pdf)