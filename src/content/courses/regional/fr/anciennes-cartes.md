---
title: Terrain
order: 3
draft: false
---

![Extrait de carte](/images/uploads/extraitrect1..png "Extrait de carte du Tumet")

### L﻿e mot du traceur, Paul Ringot :

> La course se déroulera sur les terrains karstiques du crêt du Nu, propices à la course d'orientation, avec une forêt particulièrement ouverte et agréable à courir.
>
> Les nombreux rochers, falaises et éléments de micro-relief offriront un réel challenge pour les orienteurs les plus aguerris. L'adaptation de la vitesse de déplacement sera importante sur des tracés dynamiques et variés. Les circuits les plus abordables auront la chance de traverser en fin de course la prairie surplombant la ferme du Tumet où il sera essentiel de lever la tête pour une orientation à vue et avec vue.

### A﻿nciennes cartes :

- [Le Tumet (2020)](/images/uploads/le-tumet_7500_oocup2020.png)
- [Plateau de Retord (2008)](/images/uploads/plateau-de-retord_20000_2008.jpg)

\_﻿\_\_\_\_\_\_

L﻿a carte de la zone de course a été générée à partir des données Lidar par Mapant.fr. A retrouver [ici](http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U).
<a href="http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U" > <img src="/images/uploads/mapant.png" alt="logo_MapantFR"  style="width:60%;margin:auto"></a>
