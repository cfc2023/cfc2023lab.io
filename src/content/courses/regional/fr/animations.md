---
title: Chantier participatif et animations nature
order: 4
draft: false
---

![logo_cenRA](/images/uploads/cen-rhonealpes-couleurs1.webp "Logo_cenRA")

En partenariat avec le CEN (Conservatoire des Espaces Naturels) Rhône-Alpes, plusieurs animations seront proposées pendant la journée du vendredi 19 mai !

Le matin sous forme d'atelier venez découvrir les zones humides et contribuer à la rénovation des espaces de protection **sous forme de chantier participatif, gratuit et ouvert à tous.**

L'après-midi sous forme d'échange et d'animation nature de proximité, venez découvrir les zones humides et la biodiversité sur le Retord.

\>> Inscription obligatoire sur : [Eventbrite](https://www.eventbrite.fr/e/billets-chantier-participatif-et-animations-nature-514888735067?aff=ebdsoporgprofile])
_Possibilité de s'inscrire à l'une des deux animations (ou les deux)._
