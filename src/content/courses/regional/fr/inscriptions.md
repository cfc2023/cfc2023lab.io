---
title: Inscriptions
order: 6
draft: false
---

- Pour les compétiteurs : i﻿nscriptions sur le [site de la FFCO](https://licences.ffcorientation.fr/inscriptions/2578/).

- Participants l﻿oisir : toutes les informations sur la page [Loisir](https://cfc2023.fr/fr/Loisir/).
