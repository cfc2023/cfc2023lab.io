---
title: Programme du jour
order: 1
draft: false
---
#### C﻿ourses

09:00 : Ouverture de l'accueil


11:30 : Premiers départs (compétiteurs)


12:30 - 14:30 : Départs des circuits loisir (non licenciés)


1﻿7:30 : Fermeture des circuits et débalisage

#### A﻿nimations

1﻿0:30-12:30 : Animation nature et chantier participatif (Conservatoire d'espaces naturels)

1﻿4:00-16:00 : Animation nature sur les zones humides (Conservatoire d'espaces naturels)