---
title: Listes de départ, résultats et photos
order: 0
draft: false
---

**R﻿ésultats officiels :** [par circuit](/images/uploads/resultats_régionnale.pdf)

**A﻿nalyse :** 

* GPS : [Livelox](https://www.livelox.com/Events/Show/96101/Regionale-MD-Le-Tumet-) 
* Temps intermédiaires : [WinSplits](http://obasen.orientering.se/winsplits/online/fr/default.asp?page=classes&databaseId=89649&ct=true) / [Split times (application expérimentale)](https://splittimes.pages.dev/winsplits/89649)

[**Photos**](https://photos.app.goo.gl/QhLXhqfKStWsucg77)

**Résultats live :** [Régionale](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=4)

**H﻿oraires de départ :** [par club](/images/uploads/heures-de-départ-par-club-regionaleMD.pdf) / [par circuit](/images/uploads/heures-de-départ-par-circuit-regionaleMD.pdf)

P﻿our le circuit jalonné : départ au boîtier, pas d'horaire de départ attribué. Horaire libre entre 11h30 et 16h00.