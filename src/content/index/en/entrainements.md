---
title: Training package
order: 5
draft: false
---

<a href="http://co-amberieu.ainpixel.com/page-17-ain-training-camp.html"> <img src="/images/uploads/aintrainingcampen.svg" alt="AinTrainingCampLogoEn"  style="width:100%;margin:auto"></a>

The club of CO Ambérieu propose you a training package, between 8th Avril and 11th close to the area of the races.  You can find more informations on this topic: <http://co-amberieu.ainpixel.com/page-17-ain-training-camp.html>.
