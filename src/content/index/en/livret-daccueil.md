---
title: Bulletins
order: 2
draft: false
---
D-7, Time to prepare your arrival!

The full O'Bulletin is ready, filled with plenty of useful informations to prepare your orienteering trip. Only a French ![🇨🇵](https://static.xx.fbcdn.net/images/emoji.php/v9/tc8/1/16/1f1e8_1f1f5.png) version is available, but auto-translation might do the trick (please feel free to ask if you have any [](<>)doubt).

<a href="/images/uploads/livret-d-accueil_11mai2023.pdf" > <img src="/images/uploads/txt_livretaccueil.svg" alt="Livret d'accueil" style="width: 80%; margin: auto;"></a>

In addition, the WRE bulletin 2 ![🇬🇧](https://static.xx.fbcdn.net/images/emoji.php/v9/t96/1/16/1f1ec_1f1e7.png) is available. It includes all the information related to the WRE-MD (race info, mapping details,...).


<a data-router-ignore href="/images/uploads/bulletin2_en_v1.pdf" style="display:contents;"><img src="/images/uploads/txt_bulletinwre2.svg" alt="BulletinWRE" style="height: 80%;"></a>