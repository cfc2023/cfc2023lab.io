---
title: Registering instructions
order: 6
draft: false
---
B﻿ernard, Elodie & Sylvie have worked on the registration topic to welcome you as best as possible.
To facilitate their work, we kindly ask you to:

* **Fill in your SI-numbers** on the registration form. 
* Be sure of your SI-numbers: **changes after the closing date for registration will be charged 5 euros**.
* I﻿f a runner has no SI-number filled-out, **there will be a renting fee of 5 euros**. Even if you do not need it, you will be charged for the modification of the SI-number.

P﻿lease, make only one payment per club (or one per race). 

F﻿or any question, feel free to send an email at: **inscriptions@cfc2023.fr**.

### Details for competitors (not licensed at FFCO)

According to the law #2022-296 (March 2022), it is not necessary to provide a medical certificate to take part in a competition in France, only you have to: 

##### For non-licensees over 18 years old:

* Consult and answer the questions of [this document](https://www.ffcorientation.fr/media/cms_page_media/6118/questionnaire_de_sante_FFCO_(anglais).pdf).
* Review the [10 golden rules (by the club of the cardiologists of sport)](https://www.ffcorientation.fr/media/cms_page_media/6118/10reglesd'or(FRAnglais).pdf).
* Fill in, sign and send your [health certificate](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_majeure_non-licencieeFFCO(anglais).pdf) ﻿to [inscriptions@cfc2023.fr](mailto:inscriptions@cfc2023.fr).

##### For non-licensees under 18 years old:

* Consult and answer the questions of [this document](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_mineure_non-licencieeFFCO(anglais).pdf).
* Review the [10 golden rules (by the club of the cardiologists of sport)](https://www.ffcorientation.fr/media/cms_page_media/6118/10reglesd'or(FRAnglais).pdf).
* Fill in, sign and send your [health certificate](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_mineure_non-licencieeFFCO(anglais).pdf) ﻿to [inscriptions@cfc2023.fr](mailto:inscriptions@cfc2023.fr).