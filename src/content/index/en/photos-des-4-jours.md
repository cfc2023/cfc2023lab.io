---
title: Pictures
order: 2
draft: false
---
W﻿e thank you for coming! Here are the links of the pictures taken during the 4 days:
S﻿ee you soon (OOCup 2024?)

**Pictures:** 

* [T﻿hursday - CFRS & open](https://photos.app.goo.gl/Y81iy8egVtJGT7Ne8)
* [Friday - Regional MD](https://photos.app.goo.gl/QhLXhqfKStWsucg77)
* [Saturday - CFMD & open](https://photos.google.com/share/AF1QipNbVcJ1eotZesWRGx16qVFREsZNw281920UjxWB3W0w1na3uZiTgamOoNGWgyF_ow?pli=1&key=UWM2ZTJRVFV0Wk1VREVIWkgyNXg2OVBnYm40SnhB) 
* [﻿Sunday - CFC](https://photos.app.goo.gl/MBn7yPSS2wu14gEWA)

A﻿nd last but not least: [found objects](https://photos.app.goo.gl/LUJgHaMf3UEqoWxn7).