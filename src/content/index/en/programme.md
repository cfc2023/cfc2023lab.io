---
title: "Program : 4 orienteering races!"
order: 1
draft: false
---

- 18 May: Sprint relay French championship // location: Oyonnax
- 19 May: Middle Distance - Regional // location: Plateau de Retord
- 2﻿0 May: Middle Distance French championship (WRE) // location: Plateau de Retord
- 2﻿1 May: French clubs championship (relay) // location: Plateau de Retord
