---
title: How to follow the races
order: 2
draft: false
---

F﻿or all races, there will be live results: [co-live](http://co-live.fr/)

**F﻿or the sprint relay (18/05/2023):**

* [GPS tracking](https://ffcorientation.routechoices.com/cfrs2023): 10 teams will be equiped with GPS trackers.
* [Youtube](https://www.youtube.com/@coursedorientation): Experimentation: shared views from the arena