---
title: Location of the 2 arenas
order: 4
draft: false
---

![Maps - Arenas access (region Rhône-Alpes)](/images/uploads/carte-ra.svg "Maps - Arenas access - Rhône-Alpes")

Consult [out-of-bounds areas](https://www.google.com/maps/d/edit?mid=13_PqXbx4lqPWOemV4kZ3_nNBBoKWfuU&usp=sharing).

\_﻿\_**\_﻿\_\_**

The area of the competition has been generated with Lidar data by Mapant.fr. Visit [Mapant.fr](http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U).
<a href="http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U" > <img src="/images/uploads/mapant.png" alt="logo_MapantFR" style="width:60%;margin:auto"></a>
