---
title: Entrainements
order: 5
draft: false
---

<a href="http://co-amberieu.ainpixel.com/page-17-ain-training-camp.html"> <img src="/images/uploads/aintrainingcamp.svg" alt="AinTrainingCampLogo"  style="width:100%;margin:auto"></a>

Le CO Ambérieu vous propose un camp d'entraînement, du 8 avril au 11 juin sur des terrains proches des CFC.  Retrouvez les informations sur la page dédiée : <http://co-amberieu.ainpixel.com/page-17-ain-training-camp.html>.
