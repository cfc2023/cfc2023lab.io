---
title: Livret d'accueil
order: 3
draft: false
---
J-7, l'heure de préparer votre venue en téléchargeant et lisant le livret d'accueil. Vous y trouverez des informations concernant les arenas, buvettes, parking, détails des courses (distances, cartographie, consignes de course), etc. 

<a href="/images/uploads/livret-d-accueil_11mai2023.pdf" > <img src="/images/uploads/txt_livretaccueil.svg" alt="Livret d'accueil" style="width: 80%; margin: auto;"></a>

P﻿our le WRE, retrouvez aussi le bulletin #2 (en anglais) :

<a data-router-ignore href="/images/uploads/bulletin2_en_v1.pdf" style="display:contents;"><img src="/images/uploads/txt_bulletinwre2.svg" alt="BulletinWRE" style="height: 80%;"></a>