---
title: Mot de l'accueil / inscriptions
order: 6
draft: false
---
Bernard, Élodie, Nicole et Sylvie se préparent à vous accueillir dans les meilleures conditions possibles lors des CFC.

Mais pour cela, nous avons besoin de vous et vous demandons de vous inscrire avec rigueur. 

Nos attentes :

* **Pensez à bien noter tous les numéros SI sur vos fiches d'inscription**
* Vérifiez que ce sont bien ceux que vous utiliserez car **tout changement après les dates limites de modification sur le site fédéral sera facturé 5 euros**. 
* **En cas d'absence de numéro SI, une location est facturée.** Si vous n'en avez pas besoin, cela sera considéré comme une modification de SI et sera facturé 5 euros. 

Merci aux trésoriers de centraliser les règlements par club, par **un seul virement si possible (ou à la rigueur un par course)** en  indiquant :

* le nom du club ;
* son numéro fédéral ;
* le numéro de chaque fiche d'inscription.

Si votre club ne peut pas faire de virement, nous vous enverrons une adresse postale. 

Pour toute question, vous pouvez communiquer avec nous via l’adresse suivante : **inscriptions@cfc2023.fr**.

### Détails pour l'inscription de compétiteurs non-licenciés à la FFCO

Conformément à la loi n° 2022-296 du 2 mars 2022 visant à démocratiser le sport en France, la participation à une compétition organisée par une fédération sportive n’est plus subordonnée à la présentation d’un certificat médical d’absence de contre-indication à la pratique.

Cependant, le participant non licencié FFCO à un circuit chronométré doit :

##### P﻿articipants majeurs :

* Consulter et répondre aux questions du [questionnaire de Santé FFCO](https://www.ffcorientation.fr/media/cms_page_media/6118/questionnaire_de_sante_FFCO.pdf) (majeurs).
* Prendre connaissance des [10 règles d’or édictées par le club des cardiologues du sport](https://www.ffcorientation.fr/media/cms_page_media/6118/10reglesd'or(FRAnglais).pdf).
* Enfin compléter, signer puis envoyer son [attestation de santé](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_majeure_non-licencieeFFCO.pdf) à [inscriptions@cfc2023.fr](mailto:inscriptions@cfc2023.fr).

   [](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_majeure_non-licencieeFFCO.pdf)

##### P﻿articipants mineurs :

* Consulter et répondre aux questions du [questionnaire de Santé FFCO](https://www.ffcorientation.fr/media/cms_page_media/6118/questionnaire_sante_sportif_mineur.pdf) (mineurs).
* Prendre connaissance des [10 règles d’or édictées par le club des cardiologues du sport](https://www.ffcorientation.fr/media/cms_page_media/6118/10reglesd'or(FRAnglais).pdf).
* Enfin compléter, signer puis envoyer son [attestation de santé](https://www.ffcorientation.fr/media/cms_page_media/6118/attestation_inscription_personne_mineure_non-licencieeFFCO.pdf) (mineurs) à [inscriptions@cfc2023.fr](mailto:inscriptions@cfc2023.fr).