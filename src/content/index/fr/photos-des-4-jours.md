---
title: Photos des 4 jours
order: 2
draft: false
---
U﻿n grand merci d'être venus sur ce week-end de compétition dans l'Ain. 

V﻿oici les liens des albums des 4 jours, pour garder un souvenir de votre passage chez nous ! A bientôt dans l'Ain (OOCup 2024 ?)

* [J﻿eudi - CFRS & open](https://photos.app.goo.gl/Y81iy8egVtJGT7Ne8)
* [Vendredi - Régionale MD](https://photos.app.goo.gl/QhLXhqfKStWsucg77)
* [Samedi - CFMD & open](https://photos.google.com/share/AF1QipNbVcJ1eotZesWRGx16qVFREsZNw281920UjxWB3W0w1na3uZiTgamOoNGWgyF_ow?pli=1&key=UWM2ZTJRVFV0Wk1VREVIWkgyNXg2OVBnYm40SnhB)
* [﻿Dimanche - CFC](https://photos.app.goo.gl/MBn7yPSS2wu14gEWA)

E﻿n bonus, un album photos pour les [objets trouvés](https://photos.app.goo.gl/LUJgHaMf3UEqoWxn7).
