---
title: "Au programme : 4 jours de course d'orientation "
order: 1
draft: false
---

- 18/05 : CFRS // C﻿hampionnat de France de Relais Sprint // Oyonnax et Bellignat
- 19/05 : Régionale Moyenne Distance // Plateau de Retord
- 2﻿0/05 : CFMD // Championnat de France Moyenne Distance (WRE) // Plateau de Retord
- 21/05 : CFC // Championnat de France des Clubs // Plateau de Retord
