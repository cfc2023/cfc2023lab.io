---
title: Suivi des courses
order: 2
draft: false
---

**P﻿our toutes les courses**, retrouvez les résultats en live sur [co-live](http://co-live.fr/).

**P﻿our le CFRS (relais sprint jeudi 18/05/2023) :**

* [﻿Suivi GPS](https://ffcorientation.routechoices.com/cfrs2023) (10 équipes seront équipés de GPS)
* [Youtube](https://www.youtube.com/@coursedorientation) *(images de l'arena, proposées à titre expérimental)*
