---
title: Localisation des arénas
order: 4
draft: false
---

![Carte d'accès aux 2 arénas - Rhône-Alpes](/images/uploads/carte-ra.svg "Carte d'accès - Rhône-Alpes")

C﻿onsulter les [zones interdites et accès](https://www.google.com/maps/d/edit?mid=13_PqXbx4lqPWOemV4kZ3_nNBBoKWfuU&usp=sharing)

\_﻿\_**\_﻿\_\_**

L﻿a carte de la zone de course a été générée à partir des données Lidar par Mapant.fr. A retrouver [ici](http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U).
<a href="http://mapant.fr/?fbclid=IwAR2fbErdWzBtJCBlHS2OQcD0O7SXVcnWMzqJRiFqj9FMHjxHu0XFT4vZZ0U" > <img src="/images/uploads/mapant.png" alt="logo_MapantFR"  style="width:60%;margin:auto"></a>
