---
title: Accomodations
translatedSlug: accomodations
summary: Info about accomodations for the 2023 CFC WE.
draft: false
---

# Accomodations for the 2023 CFC WE

P﻿lease note that the arena of Plateau de Retord (19-21 May) will be accessed from Col de Cuvéry (and not from Les Plans d'Hotonnes).

A﻿ single arena for the last 3 nights. Camping-car will be authorized to remain in the parking area during this time. (toilets will be provided).

![Maps - Arenas access (Rhône-Alpes)](/images/uploads/carte-ra.svg "Maps - Arenas access - Rhône-Alpes")

Find more informations about accomodations below!
