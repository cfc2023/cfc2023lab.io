---
title: Volunteers
translatedSlug: volunteers
summary: Page for Volunteers
draft: false
---
# **Volunteers**

> **Volunteers make the CFC!**


<a href="/images/uploads/cfc-2023-livret-dinformations-bénévoles.pdf" style="display:contents;"> <img src="/images/uploads/txt_livretbenevole.svg" alt="Livret Bénévoles" style="height: 80%;"></a>