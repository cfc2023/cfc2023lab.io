---
title: Leisure
translatedSlug: Leisure
summary: Page dedicated to leisure participants
draft: false
---
# L﻿eisure

### P﻿ublic races dedicated to non-licensed participant

* 1﻿8/05/2023 in Oyonnax-Bellignat: urban race
* 1﻿9-20/05/2023 in Plateau de Retord: forest races

S﻿everal circuits will be proposed, the participants choose the circuit.

### R﻿egistration

* **Urban circuits in Oyonnax – Bellignat / Thursday 18/05/2023** / Starts between 16:30 - 18:00 : [Register here!](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/inscriptions-circuits-loisirs-championnat-de-france-course-d-orientation-2023)
* **Circuits in a natural environment - Plateau de Retord / Friday 19/05/2023** / Starts between 12:30 - 14:30 : [Register here!](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/circuits-en-milieu-naturel-au-plateau-de-retord-vendredi-19-mai-2023-2)﻿
* **Circuits in a natural environment - Plateau de Retord / Saturday 20/05/2023** / Starts between 13:00 - 15:00 : [Register here!](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/circuits-en-milieu-naturel-au-plateau-de-retord-samedi-20-mai-2023-2)

T﻿he entry fees include a mandatory daily licence ("Pass'Orientation") with insurance that enable the participants to take part in events organised by FFCO.