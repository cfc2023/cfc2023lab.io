---
title: Bénévoles
translatedSlug: benevoles
summary: Page bénévoles
draft: false
---
# **B﻿énévoles**

> **Le CFC sans ses bénévoles ne serait pas le CFC !**
> Vous avez quelques heures ou quelques jours à nous offrir pour nous aider dans l'organisation et la mise en place des courses ?

<a href="/images/uploads/cfc-2023-livret-dinformations-bénévoles.pdf" style="display:contents;"> <img src="/images/uploads/txt_livretbenevole.svg" alt="Livret Bénévoles" style="height: 80%;"></a>

N﻿otes de frais : [PDF](/images/uploads/cfc-2023-note-de-frais.pdf) / [.xlsx](/images/uploads/cfc-2023-note-de-frais.xlsx)


A﻿bandon de frais :  [PDF](/images/uploads/cfc2023-abandons-de-frais.pdf) / [.xlsx](/images/uploads/cfc-2023-abandon-de-frais.xlsx)