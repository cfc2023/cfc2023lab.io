const translationKeys = {
  // NAVBAR
  "NAVBAR-news": { fr: "News", en: "News" },
  "NAVBAR-courses": { fr: "Les courses", en: "Courses" },
  "NAVBAR-language": { fr: "Langue", en: "Language" },
  "NAVBAR-language-french": { fr: "Français", en: "French" },
  "NAVBAR-language-english": { fr: "Anglais", en: "English" },
  "NAVBAR-live-results": {
    fr: "Live results des 4 jours",
    en: "Live results for 4 days",
  },
  "NAVBAR-live-cfrs-gps": {
    fr: "GPS tracking du CFRS",
    en: "CFRS live tracking",
  },
  "NAVBAR-live-cfrs-broadcast": {
    fr: "Retransmission vidéo du CFRS",
    en: "CFRS youtube broadcast",
  },

  // COURSE
  "COURSE-page-menu": { fr: "Sur cette page", en: "On this page" },

  // PARTNERS
  "PARTNERS-title": { fr: "Partenaires", en: "Partners" },

  // FOOTER
  "FOOTER-courses-section-title": { fr: "Courses", en: "Courses" },
  "FOOTER-pages-section-title": { fr: "Pages", en: "Pages" },
  "FOOTER-pages-section-admin-link": {
    fr: "Portail administrateur",
    en: "Admin portal",
  },
  "FOOTER-pages-section-welcome": { fr: "Bienvenue", en: "Welcome" },
  "FOOTER-frameworks-text-1": { fr: "Créé avec", en: "Made with" },
  "FOOTER-frameworks-text-2": { fr: "et", en: "and" },
  "FOOTER-frameworks-hosting": { fr: "Hébergé sur", en: "Hosted on" },
  "FOOTER-frameworks-source-code": { fr: "Code source :", en: "Source code:" },
};

export default translationKeys;
