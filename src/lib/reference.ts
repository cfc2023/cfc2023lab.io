export interface Reference<T> {
  get(): T;
  set(newValue: T): void;
}

export function createReference<T>(value: T): Reference<T> {
  return {
    get() {
      return value;
    },
    set(newValue: T) {
      value = newValue;
    },
  };
}
